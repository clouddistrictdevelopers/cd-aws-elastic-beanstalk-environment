output "ebs_environment_id" {
  description = "ID of the Elastic Beanstalk environment"
  value       = aws_elastic_beanstalk_environment.default.id
}

// output "security_group_id" {
//   value       = data.aws_vpc.default.id
//   description = "Security group id"
// }

// output "elb_zone_id" {
//   value       = var.elb_zone_id[var.region]
//   description = "ELB zone id"
// }

output "ec2_instance_profile_role_name" {
  value       = aws_iam_role.ec2.name
  description = "Instance IAM role name"
}

output "ebs_environment_tier" {
  description = "The environment tier"
  value       = aws_elastic_beanstalk_environment.default.tier
}

output "ebs_environment_application" {
  description = "The Elastic Beanstalk Application specified for this environment"
  value       = aws_elastic_beanstalk_environment.default.application
}

#output "ebs_environment_setting" {
#  description = "Settings specifically set for this environment"
#  value       = aws_elastic_beanstalk_environment.default.setting
#}

output "ebs_environment_all_settings" {
  description = "List of all option settings configured in the environment. These are a combination of default settings and their overrides from setting in the configuration"
  value       = aws_elastic_beanstalk_environment.default.all_settings
}

output "ebs_environment_endpoint" {
  description = "Fully qualified DNS name for the environment"
  value       = aws_elastic_beanstalk_environment.default.cname
}

output "ebs_environment_autoscaling_groups" {
  description = "The autoscaling groups used by this environment"
  value       = aws_elastic_beanstalk_environment.default.autoscaling_groups
}

output "ebs_environment_instances" {
  description = "Instances used by this environment"
  value       = aws_elastic_beanstalk_environment.default.instances
}

output "ebs_environment_launch_configurations" {
  description = "Launch configurations in use by this environment"
  value       = aws_elastic_beanstalk_environment.default.launch_configurations
}

output "ebs_environment_load_balancers" {
  description = "Elastic Load Balancers in use by this environment"
  value       = aws_elastic_beanstalk_environment.default.load_balancers
}

output "ebs_environment_queues" {
  description = "SQS queues in use by this environment"
  value       = aws_elastic_beanstalk_environment.default.queues
}

output "ebs_environment_triggers" {
  description = "Autoscaling triggers in use by this environment"
  value       = aws_elastic_beanstalk_environment.default.triggers
}
