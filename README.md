## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_elastic_beanstalk_environment.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/elastic_beanstalk_environment) | resource |
| [aws_iam_instance_profile.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.web_tier](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_security_groups"></a> [allowed\_security\_groups](#input\_allowed\_security\_groups) | List of security groups to add to the EC2 instances | `list(string)` | `[]` | no |
| <a name="input_application_subnets"></a> [application\_subnets](#input\_application\_subnets) | List of subnets to place EC2 instances | `list(string)` | n/a | yes |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Whether to associate public IP addresses to the instances | `bool` | `false` | no |
| <a name="input_availability_zone_selector"></a> [availability\_zone\_selector](#input\_availability\_zone\_selector) | Availability Zone selector | `string` | `"Any 2"` | no |
| <a name="input_ebs_app_name"></a> [ebs\_app\_name](#input\_ebs\_app\_name) | Elastic Beanstalk application name | `string` | n/a | yes |
| <a name="input_ec2_role_prefix"></a> [ec2\_role\_prefix](#input\_ec2\_role\_prefix) | Optional. Prefix to include in the EC2 role that will be created | `string` | `""` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment where app should be deployed like dev, acc or prd. | `string` | `"dev"` | no |
| <a name="input_environment_type"></a> [environment\_type](#input\_environment\_type) | Environment type, e.g. 'LoadBalanced' or 'SingleInstance'.  If setting to 'SingleInstance', `rolling_update_type` must be set to 'Time', `updating_min_in_service` must be set to 0, and `loadbalancer_subnets` will be unused (it applies to the ELB, which does not exist in SingleInstance environments) | `string` | `"LoadBalanced"` | no |
| <a name="input_http_listener_enabled"></a> [http\_listener\_enabled](#input\_http\_listener\_enabled) | Enable port 80 (http) | `bool` | `true` | no |
| <a name="input_loadbalancer_certificate_arn"></a> [loadbalancer\_certificate\_arn](#input\_loadbalancer\_certificate\_arn) | Load Balancer SSL certificate ARN. The certificate must be present in AWS Certificate Manager | `string` | `""` | no |
| <a name="input_loadbalancer_subnets"></a> [loadbalancer\_subnets](#input\_loadbalancer\_subnets) | List of subnets to place Elastic Load Balancer | `list(string)` | `[]` | no |
| <a name="input_loadbalancer_type"></a> [loadbalancer\_type](#input\_loadbalancer\_type) | Load Balancer type, e.g. 'application' or 'classic' | `string` | `"classic"` | no |
| <a name="input_project"></a> [project](#input\_project) | Name of project your app belongs to. | `string` | `""` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region to use. | `string` | `"eu-west-1"` | no |
| <a name="input_settings"></a> [settings](#input\_settings) | settings to apply to EBS | `list(map(any))` | `[]` | no |
| <a name="input_solution_stack_name"></a> [solution\_stack\_name](#input\_solution\_stack\_name) | Elastic Beanstalk stack, e.g. Docker, Go, Node, Java, IIS. For more info, see https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Additional tags | `map(string)` | `{}` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | Elastic Beanstalk Environment tier, 'WebServer' or 'Worker' | `string` | `"WebServer"` | no |
| <a name="input_version_label"></a> [version\_label](#input\_version\_label) | Elastic Beanstalk Application version to deploy | `string` | `""` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of the VPC in which to provision the AWS resources | `string` | n/a | yes |
| <a name="input_wait_for_ready_timeout"></a> [wait\_for\_ready\_timeout](#input\_wait\_for\_ready\_timeout) | The maximum duration to wait for the Elastic Beanstalk Environment to be in a ready state before timing out | `string` | `"20m"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ebs_environment_all_settings"></a> [ebs\_environment\_all\_settings](#output\_ebs\_environment\_all\_settings) | List of all option settings configured in the environment. These are a combination of default settings and their overrides from setting in the configuration |
| <a name="output_ebs_environment_application"></a> [ebs\_environment\_application](#output\_ebs\_environment\_application) | The Elastic Beanstalk Application specified for this environment |
| <a name="output_ebs_environment_autoscaling_groups"></a> [ebs\_environment\_autoscaling\_groups](#output\_ebs\_environment\_autoscaling\_groups) | The autoscaling groups used by this environment |
| <a name="output_ebs_environment_endpoint"></a> [ebs\_environment\_endpoint](#output\_ebs\_environment\_endpoint) | Fully qualified DNS name for the environment |
| <a name="output_ebs_environment_id"></a> [ebs\_environment\_id](#output\_ebs\_environment\_id) | ID of the Elastic Beanstalk environment |
| <a name="output_ebs_environment_instances"></a> [ebs\_environment\_instances](#output\_ebs\_environment\_instances) | Instances used by this environment |
| <a name="output_ebs_environment_launch_configurations"></a> [ebs\_environment\_launch\_configurations](#output\_ebs\_environment\_launch\_configurations) | Launch configurations in use by this environment |
| <a name="output_ebs_environment_load_balancers"></a> [ebs\_environment\_load\_balancers](#output\_ebs\_environment\_load\_balancers) | Elastic Load Balancers in use by this environment |
| <a name="output_ebs_environment_queues"></a> [ebs\_environment\_queues](#output\_ebs\_environment\_queues) | SQS queues in use by this environment |
| <a name="output_ebs_environment_tier"></a> [ebs\_environment\_tier](#output\_ebs\_environment\_tier) | The environment tier |
| <a name="output_ebs_environment_triggers"></a> [ebs\_environment\_triggers](#output\_ebs\_environment\_triggers) | Autoscaling triggers in use by this environment |
| <a name="output_ec2_instance_profile_role_name"></a> [ec2\_instance\_profile\_role\_name](#output\_ec2\_instance\_profile\_role\_name) | Instance IAM role name |
