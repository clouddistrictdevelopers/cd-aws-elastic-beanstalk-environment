resource "aws_elastic_beanstalk_environment" "default" {
  name        = "${var.project}-${var.environment}-${var.ebs_app_name}"
  application = var.ebs_app_name
  description = "Managed by Terraform"
  # "Node.js 12 running on 64bit Amazon Linux 2"
  solution_stack_name    = var.solution_stack_name
  wait_for_ready_timeout = var.wait_for_ready_timeout
  version_label          = var.version_label

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.ec2.name
    resource  = ""
  }
  setting {
    name      = "ListenerEnabled"
    namespace = "aws:elb:listener:80"
    value     = var.http_listener_enabled || var.loadbalancer_certificate_arn == "" ? "true" : "false"
    resource  = ""
  }
  setting {
    name      = "SSLCertificateId"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = var.loadbalancer_certificate_arn
  }
  setting {
    name      = "ListenerEnabled"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = var.loadbalancer_certificate_arn == "" ? "false" : "true"
  }
  dynamic "setting" {
    for_each = [for s in var.settings : {
      name      = lookup(s, "name", "")
      namespace = lookup(s, "namespace", "")
      value     = lookup(s, "value", "")
      resource  = lookup(s, "resource", "")
    }]

    content {
      name      = setting.value.name
      namespace = setting.value.namespace
      value     = setting.value.value
      resource  = setting.value.resource
    }
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = var.vpc_id
    resource  = ""
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "Availability Zones"
    value     = var.availability_zone_selector
    resource  = ""
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = var.associate_public_ip_address
    resource  = ""
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = join(",", sort(var.application_subnets))
    resource  = ""
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = join(",", sort(var.loadbalancer_subnets))
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "EnvironmentType"
    value     = var.environment_type
    resource  = ""
  }
}

# Move to IAM file
#
# EC2
#
data "aws_iam_policy_document" "ec2" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    effect = "Allow"
  }

  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_role" "ec2" {
  name               = "${var.project}-${var.environment}${var.ec2_role_prefix}-eb-ec2"
  assume_role_policy = data.aws_iam_policy_document.ec2.json
}

resource "aws_iam_role_policy_attachment" "web_tier" {
  role       = aws_iam_role.ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "aws_iam_instance_profile" "ec2" {
  name = "${var.project}-${var.environment}${var.ec2_role_prefix}-eb-ec2"
  role = aws_iam_role.ec2.name
}
